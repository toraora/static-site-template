import React from 'react';
import Head from 'next/head';

interface Meta {
  title?: string;
  description?: string;
  noindex?: boolean;
}

const withMeta = (meta: Meta) => ({ children }: any) => {
  console.log(meta);
  const { title, description, noindex } = meta;
  return (
    <>
      <Head>
        {title && <title>{title}</title>}
        {description && (
          <meta name="description" content={description} key="description" />
        )}
        {noindex && <meta name="robots" content="noindex" key="robots" />}
      </Head>
      {children}
    </>
  );
};

export default withMeta;

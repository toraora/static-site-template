/**
 * This file is the root of all pages. It's here we can put lang="en" on the
 * html element. We also switch between the default Document class, which
 * includes all of the scripts needed for dynamic routing and hot reloading,
 * and the custom MyDocument class used for export that strips all of this away
 * for the smallest possible deployment size.
 */

import Document, { Html, Main, NextDocumentContext } from 'next/document';

class MyDocument extends Document {
  static async getInitialProps(ctx: NextDocumentContext) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html lang="en">
        <head>
          {this.props.head}
          {this.props.styles}
        </head>
        <body>
          <Main />
        </body>
      </Html>
    );
  }
}

var ExportedDocument: any = MyDocument;

if (process.env.NODE_ENV === 'development') {
  ExportedDocument = Document;
}

export default ExportedDocument;

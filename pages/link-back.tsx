import React from 'react';
import Link from 'next/link';

const LinkBack = () => (
  <>
    <h1>This is a page of your website.</h1>

    <p>This page is written in Typescript.</p>

    <p>
      <Link href="/">
        <a>Back to home page.</a>
      </Link>
    </p>
  </>
);

export default LinkBack;

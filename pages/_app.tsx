/**
 * This file is the parent of all pages, and can take props from those pages to
 * control page behavior such as layout and meta tags.
 */

import React from 'react';
import App, { Container } from 'next/app';
import Head from 'next/head';

import Layout from '../components/layout';
import Favicon from '../components/favicon';
import GlobalStyle from '../components/global-style';

class MyApp extends App {
  static async getInitialProps({ Component, ctx }: any) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <Container>
        <GlobalStyle />
        <Head>
          <title>My Static Site</title>
          <Favicon />
        </Head>
        {pageProps.hideLayout ? (
          <Component {...pageProps} />
        ) : (
          <Layout>
            <Component {...pageProps} />
          </Layout>
        )}
      </Container>
    );
  }
}

export default MyApp;

const withTypescript = require("@zeit/next-typescript");
const withMDX = require("@next/mdx")({ extension: /\.mdx?$/ });

const config = {
  pageExtensions: ["tsx", "mdx"],
  target: "server"
};

module.exports = withTypescript(withMDX(config));

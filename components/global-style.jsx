/// <reference path="../typings/styled-jsx.d.ts" />

import React from 'react';

const GlobalStyle = () => (
  <style jsx global>
    {`
      body {
        font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI',
          'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans',
          'Helvetica Neue', sans-serif;
        line-height: 1.4;

        max-width: 800px;
        margin: 20px auto;
        padding: 0 20px;
        overflow-y: scroll;

        text-rendering: optimizeLegibility;
      }
    `}
  </style>
);

export default GlobalStyle;

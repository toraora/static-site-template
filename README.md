# `static-site-template`

Template to set up a fully static, scriptless website.

## Getting Started

1. Clone the repository into a directory of your liking.
2. Run `yarn install` to fetch all of the dependencies.
3. Run `yarn start` to start the development server.
4. Navigate to [localhost:3000/README][readme-url] to see these instructions
   rendered as a web page.

## Adding Content

This template builds upon [Next.js][next] and [MDX][mdx] to provide a platform
where

- Most content can be written in Markdown (with JSX if needed).
- Complex layouts can be handled in React (this template is set up to use
  Typescript).
- Styling is handled using [styled-jsx][styled-jsx].

### Pages

The `pages` directory represents the overall structure of your site. Any `.tsx`
or `.mdx` file under the `pages` directory gets built to a static HTML file that
gets served at that same path (for example, `pages/info/details.mdx` gets served
at `example.com/info/details`). In this manner, an `index.mdx` or `index.tsx`
file gets served as the default page for the directory it is contained in.

### Static Files

Any files placed under `static` get served behind the `/static/` path (for
example, a file `static/image.jpg` gets served as
`example.com/static/image.jpg`).

### Favicon

You can use [a favicon generator][favicon] or any other method to create a
favicon; just place it in `static` and update `components/favicon.tsx` as
necessary.

### Styles

You can add additional styles to a single page by adding a `<style jsx>` block
like so:

```
<style jsx>{`
  p {
    font-size: 2rem;
  }
`}</style>
```

It is also possible to do this in `.mdx` files, but you'll need to be aware of
how Markdown gets translated into HTML tags. See [mdx][mdx] and
[styled-jsx][styled-jsx] for more documentation.

Global styles can be specified in `components/global-style.tsx`.

### Layout

TODO

### Meta Descriptions

TODO

### Custom 404 Page

TODO

## Production Build

Run `yarn export` to bundle a fully static and minified version of your webpage
into the `dist` directory in the root of the repository. You can host the
contents of that directory using any static hosting service.

## TODOs

- Finish this documentation

[readme-url]: http://localhost:3000/README
[next]: https://nextjs.org/docs/
[mdx]: https://mdxjs.com/
[styled-jsx]: https://github.com/zeit/styled-jsx
[favicon]: https://favicon.io/favicon-generator/
